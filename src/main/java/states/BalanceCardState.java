package states;


import general.CardType;

public class BalanceCardState extends CardState{

    private int trips;
    private boolean activeStatus;

    private double balance;

    public BalanceCardState(int newId, int tripsNum){
        super(CardType.BALANCE, newId);
        this.trips = tripsNum;
        if (tripsNum>0){activeStatus = true;}
    }

    public double setBalance(double newBalance) {
        return balance = newBalance;
    }

    public double getBalance() {
        return balance;
    }

    public int getTrips() {
        return trips;
    }

    public int addTrips(int valueToAdd) {
        return trips += valueToAdd;
    }

    public int decrementTrips() {
        return trips -= 1;
    }

    public boolean setActiveStatus(boolean newStatus) {
        return activeStatus = newStatus;
    }

    public boolean getActiveStatus(){
        return activeStatus;
    }

    public int getId() {
        return id;
    }

    public CardType getCardType() {
        return cardType;
    }

}
