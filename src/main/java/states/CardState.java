package states;


import general.CardType;

public abstract class CardState {

    protected CardType cardType;
    protected int id;

    public CardState(CardType newCardType, int newId) {
        this.cardType = newCardType;
        this.id = newId;
    }

    public abstract boolean setActiveStatus(boolean newStatus);
    public abstract boolean getActiveStatus();
    public abstract int getId();
    public abstract CardType getCardType();
}
