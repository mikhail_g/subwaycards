package strategies;


public interface IProcessStrategy {

    public int processCard();

    public boolean validateCard();
}
