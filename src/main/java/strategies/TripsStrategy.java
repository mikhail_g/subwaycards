package strategies;


import general.Log;
import states.TripsCardState;

public class TripsStrategy implements IProcessStrategy {

    TripsCardState cardState;
    Log log;

    public TripsStrategy(TripsCardState tripsCardState){
        cardState = tripsCardState;
        log = Log.getInstance();
    }

    public int getTrips(){
        return cardState.getTrips();
    }

    public  int decrementTrips(){
        return cardState.decrementTrips();
    }

    public boolean setActiveStatus(boolean newStatus){
        return cardState.setActiveStatus(newStatus);
    }

    private boolean getActiveStatus(){
        return cardState.getActiveStatus();
    }

    @Override
    public boolean validateCard() {
        if(getActiveStatus()){
            if(0<getTrips()) {return true;}
            else{
                log.showMessage("No trips left, you cannot pass.");
                return setActiveStatus(false);
            }
        } else{return false;}
    }

    @Override
    public int processCard() {
        int tripsLeft = decrementTrips();
        if(1==tripsLeft) {log.showMessage("Last Trip. Enjoy!");}
        else{log.showMessage(tripsLeft + " Trips left");}
        return tripsLeft;
    }
}
