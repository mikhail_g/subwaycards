package general;

import states.CardState;
import states.RefillableCardState;
import states.TripsCardState;
import strategies.IProcessStrategy;
import strategies.RefillableStrategy;
import strategies.TripsStrategy;

public class CardProcessor {

    private IProcessStrategy iProcessStrategy;
    private Log log;

    public CardProcessor(CardState cardState){
        log = Log.getInstance();
        switch (cardState.getCardType()) {
            case TRIPS:
                iProcessStrategy = new TripsStrategy((TripsCardState)cardState);
                break;
            case REFILLABLE:
                iProcessStrategy = new RefillableStrategy((RefillableCardState)cardState);
                break;
            case BALANCE:
                break;
            case TIME:
                break;
        }
    }

    public void process(){
        if (iProcessStrategy.validateCard()){
            iProcessStrategy.processCard();
            log.showMessage("Welcome inside. Thank you for choosing subway");
        }else {
            log.showMessage("Sorry, you cannot pass");
            log.showMessage("Your card is not active");
            log.showMessage("Please add balance or bue a new card");
        }
    }
}
