package general;

import states.CardState;
import states.RefillableCardState;

import java.util.Scanner;

public class Terminal {

    RefillableCardState refillableCardState;
    double currentBalance;
    Log log;
    Scanner in = new Scanner(System.in);
    double tripPrise = 4.0;

    private static Terminal terminal = null;

    private Terminal(){
        log = Log.getInstance();
    }

    public static Terminal getInstance(){
        if(terminal == null){
            terminal = new Terminal();
        }
        return terminal;
    }

    private CardState readCard(SubwayCard subwayCard){
        log.info("reading card: " + subwayCard.toString());
        return subwayCard.getCardState();
    }

    private boolean validateCard(CardState cardState){
        return CardType.REFILLABLE.equals(cardState.getCardType());
    }

    private void showInfo(RefillableCardState refillableCardState){
        if (refillableCardState.getActiveStatus()) {
            log.showMessage("Card is active");
        } else {log.showMessage("Card is not active");}
        log.showMessage("Trips left: " + refillableCardState.getTrips());
        log.showMessage("Current balance: " + currentBalance + "uah");
    }

    private void showUserDialog(){
        log.showMessage("Insert money to add balance.");
        log.showMessage("You can: insert(i), accept(a) or exit(e)");
    }

    private void showErrorMessage(){log.showMessage("Sorry, only balance cards allowed.");}

    private void waitForUserAction(){
        while (true) {
            showUserDialog();
            String userInput = in.next();
            if (userInput.equals("insert") | userInput.equals("i")) {
                insertMoney();
            } else if (userInput.equals("accept") | userInput.equals("a")) {
                acceptInput();
                return;
//            } else if (userInput.equals("cancel") | userInput.equals("c")) {
//                cancelInput();
//                return;
            } else if (userInput.equals("exit") | userInput.equals("e")) {
                endSession();
                return;
            } else {log.showMessage("Sorry, option: " + userInput + " is not supported.");}
        }

    }

//    private void cancelInput(){
//        log.showMessage("Goodbye, thank you for came in.");
//    }

    private int addTrips(int tripsNum){
        return refillableCardState.addTrips(tripsNum);
    }

    private double setBalance(double newBalance){
        return refillableCardState.setBalance(newBalance);
    }

    private void acceptInput(){
        int resultTrips = (int)(currentBalance/tripPrise);
        double newBalance = currentBalance%tripPrise;
        addTrips(resultTrips);
        setBalance(newBalance);
        currentBalance = newBalance;
        refillableCardState.setActiveStatus(refillableCardState.getTrips()>0);
    }

    private void insertMoney(){
        log.showMessage("How match do you want to insert?");
        double enteredMoney = in.nextDouble();
        addCurrentBalance(enteredMoney);
        log.showMessage("Current balance is: " + currentBalance);
    }

    private void addCurrentBalance(double balanceToAdd){
        currentBalance=+balanceToAdd;
    }

    private void endSession(){
        refillableCardState = null;
        currentBalance = 0;
        log.showMessage("Goodbye, thank you for came in.");
    }

    public void session(SubwayCard subwayCard){
        CardState state = readCard(subwayCard);
        if (validateCard(state)){
            refillableCardState = (RefillableCardState)state;
            currentBalance = refillableCardState.getBalance();
            showInfo(refillableCardState);
            waitForUserAction();
        }else{showErrorMessage();}
    }
}
