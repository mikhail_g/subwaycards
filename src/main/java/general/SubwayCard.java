package general;

import states.CardState;

public class SubwayCard {

    private CardState cardState;

    public SubwayCard(CardState newCardState){
        this.cardState=newCardState;
    }

    public CardState getCardState(){
        return cardState;
    }

    @Override
    public String toString(){
        return getClass().getName() + '@' + Integer.toHexString(hashCode()) + '@' + getCardState().getCardType();
    }
}
