package general;

import states.CardState;

public class Turnstile {

    CardProcessor cardProcessor;
    Log log;

    private static Turnstile turnstile = null;

    private Turnstile(){
        log = Log.getInstance();
    }

    public static Turnstile getInstance(){
        if(turnstile == null){
            turnstile = new Turnstile();
        }
        return turnstile;
    }

    private CardState readCard(SubwayCard subwayCard){
        log.info("reading card: " + subwayCard.toString());
        return subwayCard.getCardState();
    }

    private void processCard(CardState cardState) {
        cardProcessor = new CardProcessor(cardState);
        cardProcessor.process();
    }

    public void checkCard(SubwayCard card){
        CardState cardState = readCard(card);
        processCard(cardState);
    }
}
