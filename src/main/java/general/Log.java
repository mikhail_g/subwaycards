package general;


public class Log {

    private static Log log = null;

    private Log(){}

    public static Log getInstance(){
        if(log == null){
            log = new Log();
        }
        return log;
    }

    public void info(String message) {
        System.out.println("[Info]:" + message);
    }

    public void warn(String message) {
        System.out.println("[Warning]:" + message);
    }

    public void showMessage(String message) {
        //TODO show message to card user
        info(message);
    }
}
