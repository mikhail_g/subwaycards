package general;

import states.BalanceCardState;
import states.CardState;
import states.RefillableCardState;
import states.TripsCardState;

public class CardsRegistry {

    private int lastID;
    private int defaultTripsNum = 3;
    private int defaultTripsForBalanceCard = 0;
    private Log log;

    private static CardsRegistry cardsRegistry = null;

    private CardsRegistry(){
        log = Log.getInstance();
    }

    public static CardsRegistry getInstance(){
        if(cardsRegistry == null){
            cardsRegistry = new CardsRegistry();
        }
        return cardsRegistry;
    }

    public SubwayCard getCardInstance(CardState cardState){
        //TODO show trips or expiration time.
        log.info(
                cardState.getCardType() + " card created with ID:  " + cardState.getId());
        return new SubwayCard(cardState);
    }

    public CardState getCardState(CardType cardType){
        switch (cardType) {
            case TRIPS:
                return getTripsCardStateInstance();
            case REFILLABLE:
                return getRefillableCardStateInstance();
            case BALANCE:
                return getBalanceCardStateInstance();
            case TIME:
                return null;
        }
        return null;
    }

    private int getNewId() {
        return lastID+1;
    }

    private CardState getTripsCardStateInstance(){
        return getTripsCardStateInstance(defaultTripsNum);
    }

    private CardState getTripsCardStateInstance(int tripsNum){
        return new TripsCardState(getNewId(), tripsNum);
    }

    private CardState getRefillableCardStateInstance(){ return getRefillableCardStateInstance(defaultTripsForBalanceCard);}

    private CardState getRefillableCardStateInstance(int tripsNum){ return new RefillableCardState(getNewId(), tripsNum);}

    private CardState getBalanceCardStateInstance(){ return getBalanceCardStateInstance(defaultTripsForBalanceCard);}

    private CardState getBalanceCardStateInstance(int tripsNum){ return new BalanceCardState(getNewId(), tripsNum);}
}
