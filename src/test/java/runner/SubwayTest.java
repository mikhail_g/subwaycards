package runner;

import general.CardType;
import general.CardsRegistry;
import general.SubwayCard;
import general.Turnstile;

public class SubwayTest {
    public static void main(String[] args) {

        CardsRegistry cardsRegistry;
        Turnstile turnstile;
        SubwayCard card;

        cardsRegistry = CardsRegistry.getInstance();
        card = cardsRegistry.getCardInstance(cardsRegistry.getCardState(CardType.TRIPS));

        turnstile = Turnstile.getInstance();
        turnstile.checkCard(card);
    }
}
