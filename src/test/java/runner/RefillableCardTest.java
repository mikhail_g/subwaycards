package runner;


import general.*;

public class RefillableCardTest {

    CardsRegistry cardsRegistry;
    Turnstile turnstile;
    Terminal terminal;
    SubwayCard card;

    public static void main(String[] args) {

        RefillableCardTest refillableCardTest = new RefillableCardTest();

        refillableCardTest.setUp();
        refillableCardTest.createCard();
        refillableCardTest.addBalance();
        refillableCardTest.tryToPass();
    }

    private void setUp(){
        cardsRegistry = CardsRegistry.getInstance();
        turnstile = Turnstile.getInstance();
        terminal = Terminal.getInstance();
    }

    private void createCard(){
        card = cardsRegistry.getCardInstance(cardsRegistry.getCardState(CardType.REFILLABLE));
    }

    private void addBalance(){
        terminal.session(card);
    }

    private void tryToPass(){
        turnstile.checkCard(card);
    }
}
