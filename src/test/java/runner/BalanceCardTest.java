package runner;


import general.*;

public class BalanceCardTest {

    CardsRegistry cardsRegistry;
    Turnstile turnstile;
    Terminal terminal;
    SubwayCard card;

    public static void main(String[] args) {

        BalanceCardTest balanceCardTest = new BalanceCardTest();

        balanceCardTest.setUp();
        balanceCardTest.createCard();
        balanceCardTest.addBalance();
        balanceCardTest.tryToPass();
    }

    private void setUp(){
        cardsRegistry = CardsRegistry.getInstance();
        turnstile = Turnstile.getInstance();
        terminal = Terminal.getInstance();
    }

    private void createCard(){
        card = cardsRegistry.getCardInstance(cardsRegistry.getCardState(CardType.BALANCE));
    }

    private void addBalance(){
        terminal.session(card);
    }

    private void tryToPass(){
        turnstile.checkCard(card);
    }
}
