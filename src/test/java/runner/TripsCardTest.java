package runner;


import general.CardType;
import general.CardsRegistry;
import general.SubwayCard;
import general.Turnstile;

public class TripsCardTest {

    CardsRegistry cardsRegistry;
    Turnstile turnstile;
    SubwayCard card;

    public static void main(String[] args) {

        TripsCardTest tripsCardTest = new TripsCardTest();

        tripsCardTest.setUp();
        tripsCardTest.createCard();
        tripsCardTest.tryToPass();
    }

    private void setUp(){
        cardsRegistry = CardsRegistry.getInstance();
        turnstile = Turnstile.getInstance();
    }

    private void createCard(){
        card = cardsRegistry.getCardInstance(cardsRegistry.getCardState(CardType.TRIPS));
    }

    private void tryToPass(){
        turnstile.checkCard(card);
    }

}
